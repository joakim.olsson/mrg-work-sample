import * as data from './all-games.json';

const entriesPerPage = 20;

const response = (data) => new Promise((resolve) => {
        setTimeout(() => {
          resolve(data)
        }, 200)
    })


const api = {
    get: {
        games: (page, filter) => {
            page = Math.max(1, page);
            let games = data.default;

            if (filter && filter.providers && filter.providers.length) {
                games = games.filter((game) => filter.providers.includes(game.gameProvider))                
            }

            if (filter && filter.collections && filter.collections.length) {
                games = games.filter((game) => {
                   
                    return game.gameCollectionIds && 
                           game.gameCollectionIds.some((collection) => filter.collections.includes(collection))
                })
            }

            const start = (page - 1) * entriesPerPage;
            const stop = start + entriesPerPage;
            const retData = games.slice(start, stop);
            return response(retData);
        },
        collections: () => {
            let collections = [];
            data.default.forEach((game) => {
                if(game.gameCollectionIds) {
                    collections = [...collections, ...game.gameCollectionIds]
                }
            });
            const retData = [...new Set(collections)].sort((a, b) => a[0] > b[0] ? 1 : -1)
            return response(retData);
        },
        providers: () => {
            const providers = data.default.map((game) => game.gameProvider)
            const retData = [...new Set(providers)].sort((a, b) => a[0] > b[0] ? 1 : -1)
            return response(retData);
        },
    }
}

export default api;
