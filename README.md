# Mr Green Recruitment Assignment - Joakim Olsson, Mpya Digital

## Project setup
Install required dependencies by running `npm i`

## Run the project
Run the project by running `npm run serve`

## About the application
The application is a SPA developed using Vue and Vuetify.
I decided to mock the API (`@/src/api`) and made a simple pagination soulution.

I inteded to implement some testing, but with the short time frame and low priority it was not implemented.

Looking forward to discuss the soulution and possible improvements!